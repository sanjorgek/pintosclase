pintosClase
===========

mis cambios en pintos para la clase de SO del profesor Peña Loza de la carrera de Ciencias de la Computacion UNAM
en equipo con Thalia Peña Netzahuatl y Jorge Santiago Alvarez Cuadros

Instalar qemu
-----------------------
~~~bash
$ sudo apt-get isntall qemu
~~~
Si al escribir *qemu* en la terminal te manda un error necesitas hacer lo sig:
~~~bash
$ sudo ln -s /usr/bin/qemu-system-i386 /usr/bin/qemu 
~~~

Configurar PintOS
-------------------------------
Descargar desde en caso de no usar los archivos proporcionados
<http://www.stanford.edu/class/cs140/projects/pintos/pintos.tar.gz>

Descomprimir el archivo y modificar el *.bashrc* como superuser:

                export PATH=/home/user/carpetas/pintos/src/utils:$PATH

Para comprobar los cambios en una terminal nueva prueba tecleando:
~~~bash
$ pintos
$ cd pintos/src/threads
$ make
$ pintos run --qemu -- run alarm-multiple
~~~

Modifica el archivo **pintos/src/utils/pintos**  y modifica la sig linea:

                $sim = "bochs" if !defined $sim;

Por:

                $sim = "qemu" if !defined $sim;

Ahora solo necesitas poner en la terminal
~~~bash
$ cd pintos/src/threads
$ pintos run run alarm-multiple
~~~

Siempre que realices cambios usa:
~~~bash
$ make clean
$ make
~~~

Para realizar pruebas usa:
~~~bash
$ make SIMULATOR=qemu check
~~~

Si modificas el archivo **pintos/src/threads/Make.vars en la linea que dice:

                SIMULATOR = --bosch

Por:

                SIMULATOR = --qemu

